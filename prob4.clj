(ns app.prob4)

(def a-sequence '(1 2 3 2 1))

(def stringseq '("abcba"))

(= a-sequence (reverse a-sequence))

(defn palindrome?
  "check whether a seq/number is a palindrome or not"
  [seq-a]
  (= (seq seq-a) (reverse seq-a)))

(palindrome? stringseq)

; make the range 100 string
(map str (range 100))

(map palindrome? (map str (range 100)))


(palindrome? "11")
(seq "11")

(take-last 2 (filter palindrome? (map str (range 100))))

;whoops, wrong. Check PRODUCT of 2-digit numbers

(take 5 (map #((take 5 (map * % (range 100)))) (range 100)))

(for [x1 (range 10)
      x2 (range 10)]
(* x1 x2))

(last (filter palindrome? (map str (for [x1 (range 1000)
                                         x2 (range 1000)]
                                     (* x1 x2)))))
;; => "90909"

;; => "90909"


(take-last 5 (filter palindrome? (map str (for [x1 (range 100 1000)
                                                x2 (range 100 1000)]
                                            (* x1 x2)))))
;; => ("282282" "119911" "906609" "514415" "580085")

(defn parse-int [s]
  (Integer/parseInt (re-find #"\A-?\d+" s)))


;; => Error printing return value (IllegalArgumentException) at clojure.lang.RT/seqFrom (RT.java:553).
;;    Don't know how to create ISeq from: java.lang.Long
(sort > (map parse-int (take-last 5 (filter palindrome? (map str (for [x1 (range 100 1000)
                                                             x2 (range 100 1000)]
                                                         (* x1 x2)))))))
;; => (906609 580085 514415 282282 119911)

;; => Execution error (ClassCastException) at app.prob4/eval8744 (form-init7192129972763313948.clj:50).
;;    class java.lang.String cannot be cast to class java.lang.Character (java.lang.String and java.lang.Character are in module java.base of loader 'bootstrap')

(first (sort > (map parse-int (take-last 5 (filter palindrome? (map str (for [x1 (range 100 1000)
                                                                              x2 (range 100 1000)]
                                                                          (* x1 x2))))))))
;; => 906609

