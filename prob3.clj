(ns app.prob3)

; look at this again later
(comment
  (defn lazy-primes
    ([] (lazy-primes 2 []))
    ([current known-primes]
     (let [factors (take-while #(<= (* % %) current) known-primes)
           remainders (map #(mod current %) factors)]
       (if (not-any? zero? remainders)
         (lazy-seq (cons
                    current
                    (lazy-primes (inc current) (conj known-primes current))))
         (recur (inc current) known-primes)))))


  (defn factorize [num]
    (loop [num num, acc [1], primes (lazy-primes)]
      (if (= num 1)
        acc
        (let [factor (first primes)]
          (if (= 0 (mod num factor))
            (recur (quot num factor) (conj acc factor) primes)
            (recur num acc (rest primes)))))))



  (factorize 13195)

;; => [1 5 7 13 29]

  (factorize 600851475143)

  (last (factorize 600851475143)))
;; => 6857

(def factors-9909 (factorize 90909))

(for [x1 factors-9909
      x2 factors-9909]
  (* x1 x2))
;; => (1
;;     3
;;     3
;;     3
;;     7
;;     13
;;     37
;;     3
;;     9
;;     9
;;     9
;;     21
;;     39
;;     111
;;     3
;;     9
;;     9
;;     9
;;     21
;;     39
;;     111
;;     3
;;     9
;;     9
;;     9
;;     21
;;     39
;;     111
;;     7
;;     21
;;     21
;;     21
;;     49
;;     91
;;     259
;;     13
;;     39
;;     39
;;     39
;;     91
;;     169
;;     481
;;     37
;;     111
;;     111
;;     111
;;     259
;;     481
;;     1369)
