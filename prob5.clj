(ns app.prob5)

;; (mod 1000 10)

;; (defn remainder? [num div]
;;   (not (= 0 (mod num div))))

;; (remainder? 5 2)
;; (remainder? 4 2)

;; (defn no-remainder? [num div]
;;   (= 0 (mod num div)))

;; (no-remainder? 2520 8)

;; (for [x1 (range 100 1000)
;;       x2 (range 1 11)]
;;      (remainder? x1 x2))

;; (filter )
;; (for [x1 (range 100 1000)
;;       x2 (range 1 11)]
;;   (remainder? x1 x2))

;; (defn [seq1]
;;   (let (for [x1 seq1
;;              x2 (range 1 11)])
;;     (remainder? x1 x2)))


;;   (filter (fn [x]
;;             (remainder? x ())))

;; (defn mod-10? [num] 
;;   (and
;;    (= 0 (mod num 1))
;;    (= 0 (mod num 2))
;;    (= 0 (mod num 3))
;;    (= 0 (mod num 4))
;;    (= 0 (mod num 5))
;;    (= 0 (mod num 6))
;;    (= 0 (mod num 7))
;;    (= 0 (mod num 8))
;;    (= 0 (mod num 9))
;;    (= 0 (mod num 10))))
;; ;; => #'app.prob5/mod-10?

;; ;; (defn mod-10? [num]
;; ;;   (if (= 0 (->> (range 1 11)
;; ;;                 (map mod num)))
;; ;;     true
;; ;;     false))

;; ;; (loop [x 10]
;; ;;   (when (> x 0)
;; ;;     (println (mod 10 x))
;; ;;     (recur (- x 1))))
;; ;; ;; => nil



;; (mod-10? 5)

;;     (last(filter mod-10? (range 0 5000)))
;; ;; => 0

;; (defn mod-10? [num]
;;   (and
;;    (= 0 (mod num 6))
;;    (= 0 (mod num 7))
;;    (= 0 (mod num 8))
;;    (= 0 (mod num 9))
;;    (= 0 (mod num 10))
;;    ))

;; (defn mod-20? [num]
;;   (and
;;    (= 0 (mod num 20))
;;    (= 0 (mod num 19))
;;    (= 0 (mod num 18))
;;    (= 0 (mod num 17))
;;    (= 0 (mod num 16))
;;    (= 0 (mod num 15))
;;    (= 0 (mod num 14))
;;    (= 0 (mod num 13))
;;    (= 0 (mod num 12))
;;    (= 0 (mod num 11))))

;; (second (filter mod-20? (range)))
;; ;; => 232792560

;; ;; => 0
;; ;;232792560

;; (range 11 21)
;; #(map %)

;; (map (partial mod 2520) (range 1 11))

;; (every? true? (map zero? (map (partial mod 232792560) (range 11 21))))

;; (defn get-mod-20 [num]
;;   (map #(= 0 %) (map (partial mod num) (range 11 21))))




;; (for [x range]
;;   (if (first (map get-mod-20 (range)))
;;              x
;;              (println "No modulus")))

;; (do (-> 232792560
;;         (partial mod (range 11 21))
;;         (map)))
;; (map zero? (map (partial mod 3) (range 11 21)))

(defn get-map-20 [num]
  (reduce #(and %1 %2)
          (map zero?
               (map
                (partial mod num)
                (range 11 21)))))


(second (filter get-map-20 (range)))


 (defn get-map-5 [num]
   (reduce #(and %1 %2)
           (map zero?
                (map
                 (partial mod num)
                 (range 1 6)))))

 (second (filter get-map-5 (range)))
;; => 60


(defn get-map-10 [num]
  (reduce #(and %1 %2)
          (map zero?
               (map
                (partial mod num)
                (range 1 11)))))

(second (filter get-map-10 (range)))
;; => 2520


